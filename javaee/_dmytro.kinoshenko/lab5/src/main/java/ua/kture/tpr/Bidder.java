package ua.kture.tpr;

import java.time.Duration;
import java.util.Random;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Slf4j
@ToString(of = {"name", "balance", "honest"})
public class Bidder {

  private Random random = new Random();

  @Getter
  private String name;

  @Getter
  private int balance = random.nextInt(50) + 20;

  private boolean honest;

  private int banTurns;

  private Bidder(String name, boolean honest) {
    this.name = name;
    this.honest = honest;
  }

  public static Bidder cheaterBidder(String name) {
    return new Bidder(name, false);
  }

  public static Bidder honestBidder(String name) {
    return new Bidder(name, true);
  }

  public Flux<Bid> bidForLot(Lot lot) {

    if (banTurns > 0) {
      log.warn("{} won't participate in bidding as it's still banned for {} turns", this, banTurns);
      banTurns--;
      return Flux.empty();
    }

    long bidsInterval = random.nextInt(5000) + 300;
    long maxBids = random.nextInt(4) + 1;

    log.info("{} is going to make max {} bids with interval of {} milliseconds",
        name, maxBids, bidsInterval);

    return Flux
        .interval(Duration.ofMillis(bidsInterval))
        .take(maxBids)
        .filter(aLong -> canMakeBid(lot))
        .map(aLong -> makeBid(lot));

  }

  private boolean canMakeBid(Lot lot) {
    if (lot.getCurrentPrice() >= balance) {

      if (honest || dontCheatThisTime()) {
        log.info("{} don't have enough money ({}) to overcome the bid {}",
            this, balance, lot.getCurrentPrice());
        return false;

      } else {
        log.warn("{} is cheating! balance {}, overcoming the bid {}",
            this, balance, lot.getCurrentPrice());
        return true;
      }

    } else {

      return true;

    }
  }

  private boolean dontCheatThisTime() {
    return random.nextBoolean();
  }

  private Bid makeBid(Lot lot) {
    int currentPrice = lot.getCurrentPrice();
    return new Bid(lot, this, upBid(currentPrice));
  }

  private int upBid(int currentPrice) {
    return currentPrice + random.nextInt(currentPrice / 3) + 1;
  }


  public boolean payForLot(Lot lot) {
    if (lot.getCurrentPrice() <= balance) {

      balance -= lot.getCurrentPrice();
      log.info("{} payed for the lot {}", this, lot);

      return true;
    } else {

      log.warn("{} refused to pay for the lot {} because of insufficient funds",
          this, lot);

      return false;
    }

  }

  public void ban(int banTurns) {
    log.warn("{} is banned for {} turns", this, banTurns);
    this.banTurns = banTurns;
  }

}
