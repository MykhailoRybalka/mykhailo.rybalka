package ua.kture.tpr;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class Bid {

  private Lot lot;
  private Bidder bidder;
  private int price;

  public Bid(Lot lot, Bidder bidder, int price) {
    this.lot = lot;
    lot.nextBid(price);
    this.bidder = bidder;
    this.price = price;
  }

}
