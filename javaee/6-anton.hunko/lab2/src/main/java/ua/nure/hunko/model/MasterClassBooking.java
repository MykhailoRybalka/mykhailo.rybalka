package ua.nure.hunko.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@NoArgsConstructor
@Getter
@Setter
@Entity
@EqualsAndHashCode
@ToString
@Table(name = "master_class_booking")
public class MasterClassBooking {

	@Id
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "master_class_id")
	private MasterClass masterClass;

	@ManyToOne
	@JoinColumn(name = "animator_id")
	private Animator animator;

	@Column(name = "start_date")
	private Timestamp startDate;
}
