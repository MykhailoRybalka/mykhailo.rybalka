package ua.nure.hunko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import ua.nure.hunko.repository.AnimatorRepository;

@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

		AnimatorRepository animatorRepository = context.getBean("animatorRepository", AnimatorRepository.class);
		animatorRepository.findAnimatorsWithoutMasterClasses().forEach(System.out::println);
	}
}
