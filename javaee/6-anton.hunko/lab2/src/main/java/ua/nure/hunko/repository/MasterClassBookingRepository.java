package ua.nure.hunko.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.nure.hunko.model.Animator;
import ua.nure.hunko.model.MasterClassBooking;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface MasterClassBookingRepository extends JpaRepository<MasterClassBooking, Integer> {

	List<MasterClassBooking> findBookingsByAnimator(Animator animator);

	@Query("from MasterClassBooking where startDate >= :from and startDate <= :to")
	List<MasterClassBooking> findBookingsBetweenDates(@Param("from") Timestamp from, @Param("to") Timestamp to);

	@Query("from MasterClassBooking where startDate >= :from")
	List<MasterClassBooking> findBookingsFrom(@Param("from") Timestamp from);

	@Transactional
	@Modifying
	@Query("update MasterClassBooking b set b.startDate = :newDate where b = :bookingToUpdate")
	void updateDateOfBooking(@Param("bookingToUpdate") MasterClassBooking masterClassBooking, @Param("newDate") Timestamp newDate);
}
