package ua.nure.hunko.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.nure.hunko.model.Direction;
import ua.nure.hunko.model.MasterClass;

import java.util.List;

public interface MasterClassRepository extends JpaRepository<MasterClass, Integer> {

	List<MasterClass> findMasterClassesByDirection(Direction direction);
}
