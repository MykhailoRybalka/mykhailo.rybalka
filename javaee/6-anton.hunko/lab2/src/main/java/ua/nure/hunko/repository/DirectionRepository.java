package ua.nure.hunko.repository;

import ua.nure.hunko.model.Direction;

import java.util.List;

public interface DirectionRepository {

	List<Direction> findAllDirections();

	Direction findDirectionById(Integer id);
}
