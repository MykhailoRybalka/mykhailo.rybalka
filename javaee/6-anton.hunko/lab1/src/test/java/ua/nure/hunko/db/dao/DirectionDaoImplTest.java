package ua.nure.hunko.db.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import ua.nure.hunko.db.ConnectionHolder;
import ua.nure.hunko.db.exception.DaoException;
import ua.nure.hunko.db.extractor.Extractor;
import ua.nure.hunko.model.Direction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DirectionDaoImplTest {

	private static final int DUMMY_INT_DATA = 1;

	@Spy
	private Direction dummyDirection;
	@Mock
	private ConnectionHolder connectionHolder;
	@Mock
	private Connection connection;
	@Mock
	private Statement statement;
	@Mock
	private PreparedStatement preparedStatement;
	@Mock
	private ResultSet resultSet;
	@Mock
	private Extractor<Direction> directionExtractor;

	@InjectMocks
	private DirectionDaoImpl underTest;

	@Before
	public void setUp() throws SQLException {
		when(connectionHolder.getConnection()).thenReturn(connection);
		when(connection.createStatement()).thenReturn(statement);
		when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
		when(statement.executeQuery(anyString())).thenReturn(resultSet);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);

		when(resultSet.next()).thenReturn(true, false);
	}

	@Test
	public void shouldFindAllDirections() throws SQLException {
		when(directionExtractor.extract(resultSet)).thenReturn(dummyDirection);

		List<Direction> directions = underTest.findAllDirections();

		assertEquals(Collections.singletonList(dummyDirection), directions);
	}

	@Test(expected = DaoException.class)
	public void shouldThrowDaoExceptionWhenExceptionOccurredWhileFindingAllDirections() throws SQLException {
		when(statement.executeQuery(anyString())).thenThrow(new SQLException());

		underTest.findAllDirections();
	}

	@Test
	public void shouldFindDirectionById() throws SQLException {
		when(directionExtractor.extract(resultSet)).thenReturn(dummyDirection);

		Direction direction = underTest.findDirectionById(DUMMY_INT_DATA);

		assertEquals(dummyDirection, direction);
	}

	@Test(expected = DaoException.class)
	public void shouldThrowDaoExceptionWhenExceptionOccurredWhileFindingDirectionById() throws SQLException {
		when(preparedStatement.executeQuery()).thenThrow(new SQLException());

		underTest.findDirectionById(DUMMY_INT_DATA);
	}
}
