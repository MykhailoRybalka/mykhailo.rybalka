package ua.nure.hunko.db.dao;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import ua.nure.hunko.db.mapper.BookingMapper;
import ua.nure.hunko.model.Animator;
import ua.nure.hunko.model.Booking;
import ua.nure.hunko.util.SqlQueries;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookingDaoImplTest {

	private static final int DUMMY_INT_DATA = 1;

	private Timestamp timestamp;

	@Spy
	private Booking booking;
	@Spy
	private Animator animator;
	@Mock
	private JdbcTemplate jdbcTemplate;
	@Mock
	private BookingMapper bookingMapper;

	@InjectMocks
	private BookingDaoImpl underTest;

	@Before
	public void setUp() {
		timestamp = new Timestamp(DUMMY_INT_DATA);

		animator = new Animator();
		animator.setId(DUMMY_INT_DATA);

		when(jdbcTemplate.query(anyString(), eq(bookingMapper), any())).thenReturn(Collections.singletonList(booking));
	}

	@Test
	public void shouldFindBookingsByDirection() {
		List<Booking> bookings = underTest.findBookingsByAnimator(animator);

		assertEquals(Collections.singletonList(booking), bookings);
	}

	@Test
	public void shouldFindBookingsBetweenDates() {
		List<Booking> bookings = underTest.findBookingsBetweenDates(timestamp, timestamp);

		assertEquals(Collections.singletonList(booking), bookings);
	}

	@Test
	public void shouldFindBookingsFrom() {
		List<Booking> bookings = underTest.findBookingsFrom(timestamp);

		assertEquals(Collections.singletonList(booking), bookings);
	}

	@Test
	public void shouldUpdateDateOfMasterClass() {
		when(jdbcTemplate.update(anyString(), eq(timestamp), any())).thenReturn(SqlQueries.ROW_UPDATED);

		boolean result = underTest.updateDateOfMasterClass(booking, timestamp);

		assertTrue(result);
	}
}
