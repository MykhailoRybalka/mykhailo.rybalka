package ua.nure.hunko.db.extractor;

import ua.nure.hunko.model.Direction;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DirectionExtractor implements Extractor<Direction> {
	@Override
	public Direction extract(ResultSet resultSet) throws SQLException {
		Direction direction = new Direction();

		direction.setId(resultSet.getInt("id"));
		direction.setName(resultSet.getString("name"));

		return direction;
	}
}
