package ua.nure.hunko.util;

public final class SqlQueries {

	public static final int ROW_UPDATED = 1;

	public static final String DIRECTION_FIND_ALL = "SELECT id, name FROM direction";
	public static final String DIRECTION_FIND_BY_ID = "SELECT id, name FROM direction WHERE id = ?";

	public static final String ANIMATOR_FIND_ALL_WITHOUT_MASTERCLASSES = "SELECT id, first_name, last_name, age FROM animator WHERE 1 > (SELECT COUNT(*) FROM master_class_booking WHERE master_class_booking.animator_id = id AND start_date > CURRENT_TIMESTAMP())";
	public static final String ANIMATOR_ADD = "INSERT INTO animator ( id, first_name, last_name, age) VALUES (?, ?, ?, ?)";
	public static final String ANIMATOR_FIND_BY_ID = "SELECT id, first_name, last_name, age FROM animator WHERE id = ?";

	public static final String MASTER_CLASS_FIND_BY_DIRECTION = "SELECT id, name, direction_id, number_of_participants FROM master_class WHERE direction_id = ?";
	public static final String MASTER_CLASS_ADD = "INSERT INTO master_class(name, number_of_participants, direction_id) VALUES (?, ?, ?)";

	public static final String BOOKING_FIND_BY_ANIMATOR = "SELECT master_class_id, animator_id, start_date FROM master_class_booking WHERE animator_id = ?";
	public static final String BOOKING_FIND_FROM = "SELECT master_class_id, animator_id, start_date FROM master_class_booking WHERE start_date >= ?";
	public static final String BOOKING_FIND_BETWEEN_DATES = "SELECT master_class_id, animator_id, start_date FROM master_class_booking WHERE start_date >= ? AND start_date <= ?";
	public static final String BOOKING_UPDATE_START_DATE = "UPDATE master_class_booking SET start_date = ? WHERE master_class_id = ?";
}
