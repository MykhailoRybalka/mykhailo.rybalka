package ua.nure.hunko.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.mariadb.jdbc.MariaDbDataSource;

import java.sql.SQLException;

@Getter
@RequiredArgsConstructor
public class DatabaseConfiguration {

	private static final String URL = "datasource.url";
	private static final String USER = "datasource.user";
	private static final String PASSWORD = "datasource.password";

	private final MariaDbDataSource dataSource;

	public DatabaseConfiguration(String fileName) throws ConfigurationException, SQLException {
		PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration(fileName);

		dataSource = new MariaDbDataSource();
		dataSource.setUrl(propertiesConfiguration.getString(URL));
		dataSource.setUser(propertiesConfiguration.getString(USER));
		dataSource.setPassword(propertiesConfiguration.getString(PASSWORD));
	}
}
