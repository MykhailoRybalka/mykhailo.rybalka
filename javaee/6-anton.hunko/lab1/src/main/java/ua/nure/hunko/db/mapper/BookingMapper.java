package ua.nure.hunko.db.mapper;

import org.springframework.jdbc.core.RowMapper;
import ua.nure.hunko.model.Animator;
import ua.nure.hunko.model.Booking;
import ua.nure.hunko.model.MasterClass;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BookingMapper implements RowMapper<Booking> {

	@Override
	public Booking mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		Booking booking = new Booking();

		MasterClass masterClass = new MasterClass();
		masterClass.setId(resultSet.getInt("master_class_id"));
		booking.setMasterClass(masterClass);

		Animator animator = new Animator();
		animator.setId(resultSet.getInt("animator_id"));

		if (booking.getAnimators() == null) {
			booking.setAnimators(new ArrayList<>());
		}
		booking.getAnimators().add(animator);
		booking.setDateOfStart(resultSet.getTimestamp("start_date"));

		return booking;
	}
}
