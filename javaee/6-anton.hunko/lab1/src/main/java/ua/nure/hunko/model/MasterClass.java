package ua.nure.hunko.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class MasterClass {

	private Integer id;
	private String name;
	private Direction direction;
	private Integer numberOfParticipants;
}
