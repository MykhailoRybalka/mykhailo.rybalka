package ua.nure.hunko.db;

import java.sql.Connection;

public class ConnectionHolder {

	private final ThreadLocal<Connection> threadLocal = new ThreadLocal<>();

	public Connection getConnection() {
		return threadLocal.get();
	}

	public void setConnection(Connection connection) {
		threadLocal.set(connection);
	}

	public void removeConnection() {
		threadLocal.remove();
	}
}
