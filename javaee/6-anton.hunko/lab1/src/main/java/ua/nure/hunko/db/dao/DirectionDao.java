package ua.nure.hunko.db.dao;

import ua.nure.hunko.model.Direction;

import java.util.List;

public interface DirectionDao {

	List<Direction> findAllDirections();

	Direction findDirectionById(Integer id);
}
