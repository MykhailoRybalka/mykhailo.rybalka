package ua.nure.hunko.db.mapper;

import org.springframework.jdbc.core.RowMapper;
import ua.nure.hunko.model.Direction;
import ua.nure.hunko.model.MasterClass;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MasterClassMapper implements RowMapper<MasterClass> {
	@Override
	public MasterClass mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		MasterClass masterClass = new MasterClass();

		masterClass.setId(resultSet.getInt("id"));
		masterClass.setName(resultSet.getString("name"));
		masterClass.setNumberOfParticipants(resultSet.getInt("number_of_participants"));

		Direction direction = new Direction();
		direction.setId(resultSet.getInt("direction_id"));
		masterClass.setDirection(direction);

		return masterClass;
	}
}
