package ua.nure.hunko.db.dao;

import ua.nure.hunko.model.Direction;
import ua.nure.hunko.model.MasterClass;

import java.util.List;

public interface MasterClassDao {

	List<MasterClass> findMasterClassesByDirection(Direction direction);

	boolean addMasterClass(MasterClass masterClass);
}
